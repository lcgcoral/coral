# CORALSharedLib()
#
# Macro to configure a directory providing a shared library.
#
# Usage:
#   CORALSharedLib([LIBS lib1 lib2...] [TESTS test1 test2...] [NO_HEADERS])
#
# LIBS: list of libraries we have to link against
# TESTS: list of directories containing tests to build
# NO_HEADERS: do not install public headers from this directory
#
# Variables with names like <test>_libs can be set before invoking the macro to
# add link libraries to a specific test.
#

include(CMakeParseArguments)

macro(CORALSharedLib)
    # Extract the arguments
    cmake_parse_arguments(ARG "NO_HEADERS" "" "LIBS;TESTS;UTILITIES" ${ARGN})
    # Get the directory name as 'package'
    get_filename_component(package ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    # Build the library
    file(GLOB ${package}_srcs src/*.cpp)
    if(NOT ${CMAKE_VERSION} VERSION_LESS 3.6.0)
      list(FILTER ${package}_srcs EXCLUDE REGEX "#")
    endif()
    add_library(lcg_${package} ${${package}_srcs})
    target_link_libraries(lcg_${package} ${ARG_LIBS})
    add_dependencies(lcg_${package} PRE_BUILD_BANNER)
    add_dependencies(POST_BUILD_BANNER lcg_${package})

    install(TARGETS lcg_${package} DESTINATION lib)

    IF(CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin") # See CORALCOOL-2819 and -2887
      cmake_policy(SET CMP0026 OLD) # Disable CMP0026 warning
      set_target_properties(lcg_${package} PROPERTIES SUFFIX ".so") # DEFAULT!
      get_property(lcg_${package}_loc TARGET lcg_${package} PROPERTY LOCATION)
      get_filename_component(lcg_${package}_we ${lcg_${package}_loc} NAME_WE)
      add_custom_command(
        TARGET lcg_${package} POST_BUILD
        COMMAND ln -sf $<TARGET_FILE_NAME:lcg_${package}> $<TARGET_FILE_DIR:lcg_${package}>/${lcg_${package}_we}.dylib
      )
      install(PROGRAMS $<TARGET_FILE_DIR:lcg_${package}>/${lcg_${package}_we}.dylib DESTINATION lib)
    ENDIF()

    include(CORALConfigScripts)
    if(NOT ARG_NO_HEADERS)
    # Copy and install headers into the build and install areas
      file(GLOB _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${package}/*.h)
      if(NOT ${CMAKE_VERSION} VERSION_LESS 3.6.0)
        list(FILTER _files EXCLUDE REGEX "#")
      endif()
      foreach(_file ${_files})
        copy_to_build(${_file} . include)
        get_filename_component(_dstdir include/${_file} PATH)
        install(FILES ${CMAKE_BINARY_DIR}/include/${_file} DESTINATION ${_dstdir})
      endforeach(_file)
    endif()

    if(ARG_TESTS)
        # Define tests
        include(CORALConfigTests)
        foreach(tst ${ARG_TESTS})
            coral_add_unit_test(${tst} PACKAGE ${package}
                                       LIBS lcg_${package} ${${tst}_libs})
        endforeach()
    endif()

    if(ARG_UTILITIES)
        # Define utilities
        include(CORALConfigUtilities)
        foreach(util ${ARG_UTILITIES})
            coral_add_utility(${util} LIBS lcg_${package} ${${util}_libs})
        endforeach()
    endif()
endmacro()
