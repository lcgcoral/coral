#ifndef CORALSERVER_IOBJECTSTOREMGR_H
#define CORALSERVER_IOBJECTSTOREMGR_H 1

// Include files
#include <map>
#include <typeinfo>
#include "CoralBase/Exception.h"
#include "CoralServerBase/ICoralFacade.h"

namespace coral
{

  namespace CoralServer
  {

    /** @class IObjectStoreMgr
     *
     *  @author Andrea Valassi
     *  @date   2007-11-26 (python)
     *  @date   2008-01-22 (C++)
     *///

    class IObjectStoreMgr
    {

    public:

      /// Destructor.
      virtual ~IObjectStoreMgr() {}

      /// Register an object in the store and return its ID.
      /// Indicate a parent which cannot be deleted before it
      /// (parentID=0 means that the object itself is a parent).
      /// Throw an exception if no parent is found with that ID.
      /// NB: Make a copy of the object, owned by the object store.
      template<class T> Token registerObject( const T& obj,
                                              Token parentID )
      {
        const std::type_info* pType = &typeid( T );
        if ( !isSupportedType( *pType ) )
          throw Exception( "Object type is not supported",
                           "IObjectStoreMgr::registerObject<T>",
                           "coral::CoralServer" );
        T* pObj = new T( obj );
        ObjectAndType oat( pObj, pType );
        return registerObjectAndType( oat, parentID );
      }

      /// Find an object in the store, given its ID.
      /// A copy of the object is returned by value.
      template<class T> T findObject( Token ID )
      {
        ObjectAndType oat = findObjectAndType( ID );
        void* pObj = oat.first;
        const std::type_info& type = *(oat.second);
        if ( type != typeid( T ) )
          throw Exception( "Object was registered with a different type",
                           "IObjectStoreMgr::findObject<T>",
                           "coral::CoralServer" );
        T* pTObj;
        try
        {
          pTObj = static_cast<T*>( pObj );
        }
        catch (...) {}
        if ( pTObj == 0 )
          throw Exception( "Static cast failed",
                           "IObjectStoreMgr::findObject<T>",
                           "coral::CoralServer" );
        // Return by value a "weak" copy of the object (CORALCOOL-2946).
        // Class T must have a method with signature "T weak()".
        return pTObj->weak();
      }

      /// Delete an object in the store, given its ID.
      /// Throw an exception if no object is found with that ID.
      virtual void releaseObject( Token ID ) = 0;

    protected:

      /// Object pointer and type.
      typedef std::pair< void*, const std::type_info* > ObjectAndType;

    private:

      /// Does the object store support this object type?
      virtual bool isSupportedType( const std::type_info& type ) = 0;

      /// Register an object in the store and return its ID.
      /// Indicate a parent which cannot be deleted before it
      /// (parentID=0 means that the object itself is a parent).
      /// Throw an exception if no parent is found with that ID.
      virtual Token registerObjectAndType( ObjectAndType oat,
                                           Token parentID ) = 0;

      /// Find an object in the store, given its ID.
      /// Throw an exception if no object is found with that ID.
      virtual ObjectAndType findObjectAndType( Token ID ) = 0;

    };

  }

}
#endif // CORALSERVER_IOBJECTSTOREMGR_H
