// Include files
#include <cerrno>
#include <cstring>
#include <limits>
#include <string>
#include <strings.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h>
#include <rpc/pmap_prot.h>
#include <rpc/xdr.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "crc32.h"
#include "CoralServerBase/portmap.h"
#include "CoralServerBase/CoralServerBaseException.h"

// Logger
#define LOGGER_NAME "CoralServerBase::portmap"
#include "CoralServerBase/logger.h"

namespace {

class PortmapException : public coral::CoralServerBaseException {
public:
    PortmapException(const std::string& msg) :
        coral::CoralServerBaseException(msg, "pmapRegister", "CoralServerBase")
    {}
};

}

std::pair<uint32_t, uint32_t> coral::parsePort(const std::string& portSpec)
{
    const char* portStr = portSpec.c_str();
    if (portStr[0] == '[') {
        // must be RPC thing
        char *endptr = 0;
        unsigned long prog = 0, version = 1;

        prog = strtoul(portStr+1, &endptr, 0);
        if (endptr != portStr+1) {
            if (*endptr == '.') {
                // version number follows
                char* vstr = endptr+1;
                if (*vstr == '#') {
                    ++ vstr;
                    size_t d = strlen(vstr);
                    if (d == 0) {
                        // nothing after hash?
                        return std::make_pair(prog, version);
                    }
                    endptr = vstr + d - 1;
                    version = Crc32_ComputeBuf(0U, vstr, d-1);
                } else {
                    version = strtoul(vstr, &endptr, 0);
                    if (endptr == vstr) {
                        // cannot parse any character after dot
                        return std::make_pair(uint32_t(0), uint32_t(0));
                    }
                }
            }
            if (endptr[0] == ']' && endptr[1] == '\0') {
                return std::make_pair(prog, version);
            }
        }
    } else {
        char *endptr = 0;
        unsigned long port = strtoul(portStr, &endptr, 0);
        // check range too
        if (*endptr == '\0' && port <= std::numeric_limits<unsigned short>::max()) {
            return std::make_pair(uint32_t(0), uint32_t(port));
        }
    }

    return std::make_pair(uint32_t(0), uint32_t(0));
}

int coral::pmapRegister(unsigned short port, unsigned long rpc_prognum,
                        unsigned long rpc_version, bool override,
                        const std::string& lockDir)
{
    int fd = -1;

    // file locking
    if (! lockDir.empty()) {
        std::string path = lockDir + "/coral-pmap-lock-" + std::to_string(rpc_prognum) +
                "." + std::to_string(rpc_version);
        // we want lock file to be used by any uid so set permissions
        auto old_mask = umask(0);
        fd = open(path.c_str(), O_CREAT|O_RDWR, 0666);
        // umask does not change errno, but just in case copy it
        int err = errno;
        umask(old_mask);
        if (fd < 0) {
            std::string msg = "Failed to create/open lock file ";
            msg += path;
            msg +=  ", error: ";
            msg +=  strerror(err);
            if (override) {
                // we are going to ignore lock in any case so only complain but proceed
                msg += ", override is on - continue without lock";
                logger << Warning << msg << endlog;
            } else {
                throw PortmapException(msg);
            }
        }
        if (fd >= 0) {
            // try to get exclusive lock
            if (flock(fd, LOCK_EX|LOCK_NB) < 0) {
                char buf[16];
                auto bytes = read(fd, buf, sizeof buf);
                // ignore error
                if (bytes < 0) bytes = 0;
                std::string msg = "Lock file ";
                msg += path;
                msg += " is locked by PID:port=";
                msg += std::string(buf, buf+bytes);
                if (override) {
                    // ignore lock if override is on, only complain but proceed
                    msg += ", override is on - continue without lock";
                    logger << Warning << msg << endlog;
                } else {
                    throw PortmapException(msg);
                }
            } else {
                // lock is successful, store my PID in file
                ftruncate(fd, 0);
                std::string contents = std::to_string(getpid()) + ":" + std::to_string(port);
                write(fd, contents.data(), contents.size());
                logger << Debug << "Locked file " << path << " with PID:port=" << contents << endlog;
            }
        }
    }

    // if we have lock on file or override is set then remove any existing mapping
    if (override || fd >= 0) {
        pmapUnregister(rpc_prognum, rpc_version);
    }

    // try to register
    bool rc = pmap_set(rpc_prognum, rpc_version, IPPROTO_TCP, port);
    if (! rc) {
        // get existing port maping
        unsigned short oldport = pmapGetPort(INADDR_LOOPBACK, rpc_prognum, rpc_version);
        std::string msg = "Cannot register new binding because old binding exists [";
        msg += std::to_string(rpc_prognum);
        msg += '.';
        msg += std::to_string(rpc_version);
        msg += "] -> ";
        msg += std::to_string(oldport);
        throw PortmapException(msg);
    }

    return fd;
}

bool coral::pmapUnregister(unsigned long rpc_prognum, unsigned long rpc_version,
                           unsigned short port, int lockfd)
{
    bool doUnreg = true;
    unsigned short reg_port = pmapGetPort(INADDR_LOOPBACK, rpc_prognum, rpc_version);
    if (reg_port == 0) {
        // nothing is registered, skip everything (but do file unlocking)
        doUnreg = false;
    } else {
        if (port != 0 && port != reg_port) {
            // ports don't match, means someone registered after us
            // keep their registration but unlock lock file
            std::string msg = "pmapUnregister - currently registered port (";
            msg += std::to_string(reg_port);
            msg += ") is different from application port (";
            msg += std::to_string(port);
            msg += "), will skip unregistration for ";
            msg += std::to_string(rpc_prognum);
            msg += ".";
            msg += std::to_string(rpc_version);
            logger << Warning << msg << endlog;
            doUnreg = false;
        }
    }

    if (doUnreg) {
        // de-register
        bool rc = pmap_unset(rpc_prognum, rpc_version);
        if (! rc) {
            // need to unlock the file in any case
            if (lockfd >= 0) close(lockfd);
            throw PortmapException("pmap_unset failed, RPC failure");
        }
    }

    // unlock lock file after de-registration
    if (lockfd >= 0) close(lockfd);
    return doUnreg;
}

unsigned short coral::pmapGetPort(uint32_t host_ip, unsigned long rpc_prognum, unsigned long rpc_version)
{
    // address for machine with portmapper
    struct sockaddr_in addr;
    bzero(&addr, sizeof addr);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(host_ip);
    addr.sin_port = htons(PMAPPORT);

    // pmap_getport() can return port number but it has one peculiarity -
    // if exact version number does not exist in portmapper it returns port
    // for a random version number instead. Here we need exact match so we
    // resort to pmap_getmaps() and doing manual match.
    auto pmap = pmap_getmaps(&addr);
    unsigned short port = 0;
    for (auto map = pmap; map; map = map->pml_next) {
        const auto& pml_map = map->pml_map;
        if (pml_map.pm_prog == rpc_prognum
            && pml_map.pm_vers == rpc_version
            && pml_map.pm_prot == IPPROTO_TCP) {
            port = pml_map.pm_port;
            break;
        }
    }

    // pmap needs to be freed otherwise we'll leak memory. Man pages say
    // nothing about it but I saw few examples when people used xdr_free()
    // which I think should be the right way to do it.
    xdr_free((xdrproc_t) xdr_pmaplist, (caddr_t) &pmap);

    return port;
}

unsigned short coral::pmapGetPort(const std::string& host, unsigned long rpc_prognum, unsigned long rpc_version)
{
    // resolve host name
    struct addrinfo hints, *res0;
    bzero((char *) &hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    int error = getaddrinfo(host.c_str(), NULL , &hints, &res0);
    if (error != 0) {
        std::string message = "Failed to resolve host name \"";
        message += host;
        message += "\": ";
        message += gai_strerror(error);
        throw PortmapException(message);
    }
    uint32_t addr = ntohl(((struct sockaddr_in*)(res0->ai_addr))->sin_addr.s_addr);
    freeaddrinfo(res0);

    return pmapGetPort(addr, rpc_prognum, rpc_version);
}
