#include <unistd.h> // for usleep
#include "ConnectionPoolTimer.h"
#include "ConnectionPool.h"
#include "CoralCommon/Utilities.h"

//-----------------------------------------------------------------------------

coral::ConnectionService::ConnectionPoolTimer::ConnectionPoolTimer()
  : m_pool(0)
{
}

//-----------------------------------------------------------------------------

coral::ConnectionService::ConnectionPoolTimer::ConnectionPoolTimer( ConnectionPool& pool )
  : m_pool(&pool)
{
}

//-----------------------------------------------------------------------------

coral::ConnectionService::ConnectionPoolTimer::~ConnectionPoolTimer()
{
}

//-----------------------------------------------------------------------------

void
coral::ConnectionService::ConnectionPoolTimer::operator()()
{
  if( m_pool )
  {
    while( m_pool->waitForTimeout() )
    {
      m_pool->cleanUpTimedOutConnections();
      //usleep(100000); // wait at least 0.1s (sanity hack for CORALCOOL-1584)
    }
  }
}

//-----------------------------------------------------------------------------
