#ifndef CORALCOMMON_IDEVSESSIONPROPERTIES_H
#define CORALCOMMON_IDEVSESSIONPROPERTIES_H 1

#include <string>

namespace coral
{

  namespace monitor
  {
    class IMonitoringService;
  }

  class IDevSessionProperties
  {

  public:

    /// Returns the domain service name
    virtual const std::string& domainServiceName() const = 0;

    /// Returns the connection string
    virtual std::string connectionString() const = 0;

    /// Sets the monitoring service
    virtual void setMonitoringService( coral::monitor::IMonitoringService* monitoringService ) = 0;

    /// Returns the monitoring service
    virtual coral::monitor::IMonitoringService* monitoringService() const = 0;

  };

}
#endif // CORALCOMMON_IDEVSESSIONPROPERTIES_H
