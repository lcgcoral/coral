#ifndef CORALBASE_CORALTHREADHEADERS_H
#define CORALBASE_CORALTHREADHEADERS_H 1

// TEMPORARY! Keep using boost::thread until issues are fixed (CORALCOOL-1584)
// [NB May use std::mutex even when using boost::thread, keep these separate!]
// [NB Macro CORAL_HAS_CPP11_THREADS is used in CORAL_SERVER for thread_group]
#undef CORAL_HAS_CPP11_THREADS
//#define CORAL_HAS_CPP11_THREADS 1 // Uncomment out to use boost::thread again

// Include files
// FIXME! This is only needed for boost::thread now, should use std::thread...
// NB: boost_thread_headers should be _completely_ removed in CORAL3 branch!
#ifndef CORAL_HAS_CPP11_THREADS
// TEMPORARY! Keep using boost::thread until issues are fixed (CORALCOOL-1584)
#include "CoralBase/../src/boost_thread_headers.h" // SHOULD BE REMOVED!
#else
// Eventual new implementation based on std::thread (CORALCOOL-1584)
#include <thread>
#endif

// Typedef coral::thread for CORAL internals (task #50199)
// [NB thread_group was moved to CORAL_SERVER/CoralSockets, its only user]
namespace coral
{
#ifndef CORAL_HAS_CPP11_THREADS
  typedef boost::thread thread;
  namespace this_thread = boost::this_thread;
#else
  typedef std::thread thread;
  namespace this_thread = std::this_thread;
#endif
}

#endif // CORALBASE_CORALTHREADHEADERS_H
